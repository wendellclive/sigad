package br.gov.jfrj.siga.ex;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@Embeddable
public class ExDocumentoUltimoExpedienteId implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name = "orgao_id", nullable = false)
	private Long orgaoId;

	@NotNull
	@Column(name = "forma_documento_id", nullable = false)
	private Long formaDocumentoId;

	@NotNull
	@Column(name = "ano_emissao ", nullable = false)
	private Long anoEmissao;

	@NotNull
	@Column(name = "numero_sequencial", nullable = false)
	private Long numeroSequencial;

	protected ExDocumentoUltimoExpedienteId() {
	}

	public ExDocumentoUltimoExpedienteId(Long orgaoId, Long formaDocumentoId, Long anoEmissao, Long numeroSequencial) {
		this.orgaoId = orgaoId;
		this.formaDocumentoId = formaDocumentoId;
		this.anoEmissao = anoEmissao;
		this.numeroSequencial = numeroSequencial;
	}

	@Override
	public int hashCode() {
		return Objects.hash(anoEmissao, formaDocumentoId, numeroSequencial, orgaoId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ExDocumentoUltimoExpedienteId)) {
			return false;
		}
		ExDocumentoUltimoExpedienteId other = (ExDocumentoUltimoExpedienteId) obj;
		return Objects.equals(anoEmissao, other.anoEmissao)
				&& Objects.equals(formaDocumentoId, other.formaDocumentoId)
				&& Objects.equals(numeroSequencial, other.numeroSequencial)
				&& Objects.equals(orgaoId, other.orgaoId);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
